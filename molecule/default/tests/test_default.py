from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_files(host):
    files = [
        "/etc/systemd/system/node-exporter.service",
        "/usr/local/bin/node_exporter"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

def test_user(host):
    assert host.group("nodeexporter").exists
    assert "nodeexporter" in host.user("nodeexporter").groups
    assert host.user("nodeexporter").shell == "/usr/sbin/nologin"

def test_service(host):
    s = host.service("node-exporter")
    assert s.is_enabled
    assert s.is_running

def test_socket(host):
    sockets = [
        "tcp://127.0.0.1:9100"
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
