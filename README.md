Prometheus Node Exporter
=========
Installs and Prometheus Node Exporter.  

Include role
------------
```yaml
- name: node-exporter  
  src: https://gitlab.com/ansible_roles_v/node-exporter/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - node-exporter
```